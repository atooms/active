"""
**atooms.active** - a simulation backend for active matter systems. 
"""

from .vicsek import *
from .neighbors import *
