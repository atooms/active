Basics
------

**active** is a backend for the **atooms** framework. For this reason, it is strongly recommended to have prior knowledge of how atooms works. A good primer is the `tutorial <https://atooms.frama.io/atooms/tutorial/>`_ provided as part of the docs.

Active matter systems
~~~~~~~~~~~~~~~~~~~~~
Before delving into the technical aspects of **active**, you will need to have some previous knowledge on active matter and the Vicsek model. `This <https://arxiv.org/abs/1010.5017>`_ paper from Vicsek et al provides a fairly comprehensive overview on collective motion in active matter.

System setup
~~~~~~~~~~~~
We start by importing all of the needed packages, mainly **atooms** and **numpy**, and setting up a 2D atooms system composed by N particles

.. code:: python

        import numpy
        import atooms
        from atooms.system import System
        from atooms.system.cell import Cell

        dim = 2
        system = System(N = 200)


Let us list the key ingredients needed to properly setup a system. First, we need a simulation cell of side L

.. code:: python

        L = 32.
        system.cell = Cell(np.ones(dim)*L)


Each particle needs a position, an orientation and a starting velocity, which we will initialize uniformely using numpy. Particle orientations are taken between :math:`[-\pi, \pi]`. Furthermore, periodic boundary conditions are set up to avoid major finite size effects; we use the fold method provided by atooms.
Note: we are effectively considering point-like particles by setting the radius to None. 

.. code:: python

        for p in system.particle:
                p.position = [L*np.random.uniform() for i in range(dim)]
                p.velocity = np.zeros(dim)
                p.orientation = np.random.uniform(-np.pi, np.pi)
                p.radius = None
                p.fold(system.cell)


These are the essential particle properties needed for basic simulations. Other particle properties can be easily added without loss of functionality

.. code:: python

        p.charge = 1.0


Vicsek model 
~~~~~~~~~~~~

Currently, **active** only includes the well-known Vicsek model, along with some of its main variations. Details on the model can be found in `Vicsek et al. (1995) <https://doi.org/10.1103/PhysRevLett.75.1226>`_ and in later works, such as `Grégoire and Chaté (2004) <https://doi.org/10.1103/PhysRevLett.92.025702>`_.
Two core variations of the Vicsek model are available,

* *scalar*, based on the work done by Vicsek et al. (1995);
* *vectorial*, based on the 2004 paper by Grégoire and Chaté.

Both variations employ three main control parameters: the noise amplitude :math:`\eta`; the velocity magnitude :math:`\v_0`; the system density :math:`\rho`. 

Running a bare-bones simulation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Suppose we want to simulate a system according to the scalar variation of the Vicsek model. To do so, we employ the ``Vicsek`` simulation backend provided by **active**, as shown in the following example.

.. code:: python 

        from atooms.active.vicsek import Vicsek
        from atooms.active.neighbors import VicsekNeighbors
        from atooms.simulation import Simulation

        eta = 0.4       # noise amplitude
        v0 = 0.5        # velocity magnitude
        
        # Let's use the system defined in the previous snippets

        backend = Vicsek(system, eta, v0, noise='vectorial')
        neighbors = VicsekNeighbors(system, method='kdtree')
        bck.neighbors = neighbors

        Simulation(backend).run(10)


Some keyword arguments we can pass to the ``Vicsek`` backend are

* ``radius``: radius of the circular area used to fetch each particle's nearest neighbors;
* ``deltat``: size of the simulation time step.

Neighbor search algorithms
~~~~~~~~~~~~~~~~~~~~~~~~~~

We have a few different choices regarding how to fetch each particle's neighbors in a fixed radius. Two main approaches are available,

* ``kdtree``: the `kD-Tree <https://en.wikipedia.org/wiki/K-d_tree#Nearest_neighbour_search>`_ nearest neighbor algorithm (fastest);
* ``f90``: a brute force approach, which uses a fast Fortran kernel to compute the neighbors' positions and orientations.

As we saw in the previous example, a specific algorithm can be specified using the keyword argument ``method`` when calling the ``VicsekNeighbors`` neighbor search kernel.

