# Changelog

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html). This file only reports changes that increase major and minor versions, as well as deprecations and critical bug fixes.

## 1.0.3 - 2023/06/16

[Full diff](https://framagit.org/activematter/active/-/compare/1.0.2...1.0.3)


Minor changes and fixes to `api.py`. Move package into `atooms` namespace and add to PyPI.

### Changes
- Add all significant metadata to trajectory files
- Add UID to trajectory file name when filename isn't user specified

 
### Fixes
- Fix metadata log path directory 
- Revert removal of user specified `data_log` path variable

## 1.0.2 - 2023/06/11

[Full diff](https://framagit.org/activematter/active/-/compare/1.0.1...1.0.2)


Create public API documentation using `pdoc3` and automate it using CI. Minor changes to `api.py`.

### Changes
- Add simulation parameters logging to file with UniqueIDentifier in `api.py`
- Add RNG seed read from file in `api.py`
- Add public API via `pdoc3` at `active/api/atooms/active/`
- Add docstrings to every module
- Move tutorial to `active/tutorial`

### Removals
- Remove `data_log` from `vm` in `api.py` (now parameter logging defaults to `file_out + '.param'` path)

### Fixes
- Fix format issues in reST tutorial files
- Fix `pages` CI
- Fix package imports in `api.py`
- Revert to `alabaster` theme in tutorial

## 1.0.1 - 2023/06/06

[Full diff](https://framagit.org/activematter/active/-/compare/1.0.0...1.0.1)

### New features
- Add system parameters (_e.g._ $\eta, \rho, \dots$) to API output trajectory file metadata

## 1.0.0 - 2023/05/15

[Full diff](https://framagit.org/activematter/active/-/compare/0.9.0...1.0.0)

### New features
- Add bare-bones API
- Add default neighbor search function
- Add basic documentation
### Fixes
- Fix `box` bug in `VicsekNeighbors`: pass `cell.side` instead of `cell.volume` to `neighbors.compute()`
- Fix dimensionality in `fold()` method in `Vicsek`
### Changes
- Drop useless or dead arguments in `VicsekNeighbors` and `Vicsek` interfaces
### Removals
- Remove`timer_to_file` method from `Vicsek`
