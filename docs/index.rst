active
======

**active** is a simulation backend for `atooms <https://framagit.org/atooms/atooms>`_, a high-level framework for particle-based simulations. This backend implements active matter systems, e.g. the Vicsek model as described in `Vicsek et al. (1995) <https://doi.org/10.1103/PhysRevLett.75.1226>`_ and in `Grégoire and Chaté (2004) <https://doi.org/10.1103/PhysRevLett.92.025702>`_.

These pages aim to showcase some basic features of **active**.

.. toctree::
    :maxdepth: 2
    :caption: Features
    
    basics
    

