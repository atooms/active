#!/usr/bin/env python

import numpy
import unittest
from atooms.system import System
from atooms.system.cell import Cell
from atooms.system.particle import Particle

try:
    from atooms.active.neighbors import VicsekNeighbors
except:
    # This allows running the test within the tests folder
    import sys
    sys.path.append('../atooms/active/')
    from neighbors import VicsekNeighbors

class Test(unittest.TestCase):

    def setUp(self):
        import random
        self.n = 200
        random.seed(1)
        numpy.random.seed(1)
        self.system = System(N=self.n)
        self.system.density = 0.6
        for p in self.system.particle:
            p.orient = numpy.random.uniform(-numpy.pi / 2, numpy.pi / 2)

    def test_neighbors_methods(self):

        pos = self.system.view('pos', order='F')
        ori = self.system.view('orient', order='F')

        fortran_neigh = VicsekNeighbors(self.system, method='f90')
        kd_neigh = VicsekNeighbors(self.system, method='kdtree')
        sum_f90, num_f90 = fortran_neigh.compute(self.system.cell.side, pos, ori)
        sum_kdt, num_kdt = kd_neigh.compute(self.system.cell.side, pos, ori)

        for x, y in zip(num_kdt, num_f90):
            self.assertEqual(x, y)
        for x, y in zip(numpy.angle(sum_kdt), numpy.angle(sum_f90)):
            self.assertEqual(x, y)

    def test_neighbors_f90_variations(self):

        pos = self.system.view('pos', order='F')
        ori = self.system.view('orient', order='F')
        ids = numpy.array([1.0 for i in range(self.n)], order='F')

        std_neigh = VicsekNeighbors(self.system, method='f90', f90file='neighbor_list.f90')
        newt_neigh = VicsekNeighbors(self.system, method='f90', f90file='neighbor_list_newton.f90')
        inline_neigh = VicsekNeighbors(self.system, method='f90', f90file='neighbor_list_newton_inline.f90')

        sum_std, num_std = std_neigh.compute(self.system.cell.side, pos, ori)
        sum_new, num_new = newt_neigh.compute(self.system.cell.side, pos, ori)
        sum_inl, num_inl = inline_neigh.compute(self.system.cell.side, pos, ori)
        for x, y, z in zip(num_std, num_new, num_inl):
            self.assertEqual(x, y, z)
        for x, y, z in zip(numpy.angle(sum_std), numpy.angle(sum_new), numpy.angle(sum_inl)):
            self.assertEqual(x, y, z)

    """
    def test_orientation(self):
        from vicsek import neighborf90

        s = System(N=5, d=2)

        s.particle[0].position[:] = [0, 0]
        s.particle[1].position[:] = [1, 0]
        s.particle[2].position[:] = [0, 1]
        s.particle[3].position[:] = [-1, 0]
        s.particle[4].position[:] = [0, -1]
        s.cell.side[:] = [20, 20]  # o [20, 20] per eliminare le pbc
        s.particle[0].orient = 0.
        s.particle[1].orient = 0.
        s.particle[2].orient = 0.5 * numpy.pi
        s.particle[3].orient = numpy.pi
        s.particle[4].orient = 1.5 * numpy.pi

        pos = s.view('pos', order='F')
        ori = s.view('orient', order='F')

        neighbors, noneighbors, void4 = neighborf90(s, 1.0, pos, ori)
        neighbors[neighbors == 0.] = numpy.nan

        avg = numpy.angle(numpy.nansum(numpy.exp(1j*neighbors), axis = 1))             #mimicking Gregoire, Chaté
        avg[noneighbors] = numpy.angle(numpy.exp(1j*ori[noneighbors]))
        #avg = numpy.mean(ori_neighbors, axis=1)                                   #mimicking Vicsek
        #avg[noneighbors] = orientations[noneighbors]

        expected = numpy.pi
        print(neighbors)
        self.assertEqual(expected,avg[0])
       """


if __name__ == '__main__':
    unittest.main()
